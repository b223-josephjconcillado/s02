<?php
// [SECTION] Repetition Control Structures
// while loop
function whileLoop(){
    $count = 5;
    while($count !== 0){
        echo $count . '<br/>';
        $count--;
    }
}
// do while loop
function doWhileLoop(){
    $count = 20;
    do {
        echo $count .'<br/>';
        $count --;
    } while ($count > 0);
}
// for loop
function forLoop(){
    for($count=0; $count<=10; $count++){
        echo $count .'<br/>';
    }
}

// continue and break
function modifiedForLoop(){
    for($count=0;$count<=20;$count++){
        if($count % 2 === 0){
            continue;
        }
        echo $count .'<br/>';
        if($count > 10) {
            break;
        }
    }
}

// [SECTION] Array Manipulation
$studentNumbers = array('2020-1923','2020-1924','2020-1925','2020-1926','2020-1927'); //before PHP 5.4
$studentNumbers = ['2020-1923','2020-1924','2020-1925','2020-1926','2020-1927']; //after PHP 5.4

// Simple Array
$grades = [98.5,94.3,89.2,90.1];
$computerBrands = ['Acer','Asus','Lenovo','Neo','Redfox','Gateway','Toshiba','Fujitsu'];

// Associative Array
$gradesPeriods = [
    'firstGrading'=> 98.5,
    'secondGrading' => 94.3,
    'thirdGrading' => 89.2,
    'fourthGrading' => 90.1
];

// Multidimensional Array ( Two Dimensional)
$heroes = [['ironman','thor','hulk'],['wolverine','cyclops','jean grey'],['batman','superman','wonderwoman']];

// 2 dimensional associative array
$ironManPowers = [
    'regular'=>['repulsor blast','rocket punch'],
    'signature'=>['unibeam']
];

// Sorting Array mutators
$sortedBrands = $computerBrands;
$reverseSortedBrands = $computerBrands;

// Ascending Order
sort($sortedBrands);
// Descending Order
rsort($reverseSortedBrands); //https://www.php.net/manual/en/ref.array.php

// Search Function
function searchBrand($brands,$brand){
    // in_array($searchValue,$arrayList)
    return (in_array($brand,$brands) ? "$brand is in the array": "$brand is not in the array");
}

$reversedGradePeriods = array_reverse($gradesPeriods);
$gradePeriodsCopy = $gradesPeriods;
// sort($gradePeriodsCopy);
// ksort($gradePeriodsCopy);
asort($gradePeriodsCopy);
?>